import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state= {
      pictures: [],
    };
  }

  componentDidMount(){
    fetch('https://randomuser.me/api?results=2')
    .then(results => {
      return results.json();
    }).then(data =>{
     let pictures = data.results.map((pic) => {
        return(
          <div key={pic.results}>
            <img src={pic.picture.medium}/>
          </div>
        )
     })
     this.setState({pictures: pictures});
     console.log("state", this.state.pictures);
    })
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">React-Rest-Demo</h1>
        </header>
        <div className="container2">
          {this.state.pictures}
        </div>
      </div>
    );
  }
}

export default App;
